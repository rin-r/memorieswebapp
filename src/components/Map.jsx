import React, { useEffect, useRef, useState } from 'react';
import L from 'leaflet';


function Map({ coordinates }) {

    const mapReference = useRef(null);

    useEffect(() => {

        const map = L.map(mapReference.current).setView(coordinates, 16);

        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        }).addTo(map);

        L.Icon.Default.imagePath = 'leaflet/dist/images/';
        var icon = L.icon({
            iconUrl: 'marker-icon.png',
            shadowUrl: 'marker-shadow.png',

            iconSize: [25, 41],
            shadowSize: [30, 65],
            iconAnchor: [12, 41],
            shadowAnchor: [7, 65]
        });

        L.marker(coordinates, { iconUrl: icon }).addTo(map);

        return () => {
            map.off();
            map.remove();
        };
    }, []);

    return (
        <div className="" id="map" ref={mapReference} style={{ height: "100%", minHeight: "200px" }}></div>
    )
}

export default Map;