import React from 'react';

function Carousel({ data }) {

    return (
        <div className="carousel slide" id="carouselAbout" data-bs-ride="carousel">

            <div className="carousel-indicators">
                {
                    data.map((carousel, index) => (
                        <button key={index} data-bs-target="#carouselAbout" className={index === 0 ? "active" : ""} aria-current={index === 0 ? "true" : "false"} data-bs-slide-to={index} aria-label={"Slide " + index}></button>
                    ))
                }
            </div>

            <div className="carousel-inner">
                {
                    data.map((carousel, index) => (
                        <div key={index} className={index === 0 ? 'carousel-item active' : 'carousel-item'} style={ {height: '500px',} }>
                            <img src={carousel.imgSource} className="d-block w-100" style={ {objectFit: "cover", minHeight: "500px"} } alt={carousel.imgAlt} />
                            <div className="carousel-caption d-block">
                                <h5>{carousel.label}</h5>
                                <p>{carousel.description}</p>
                            </div>
                        </div>
                    ))
                }
            </div>
            
        </div>
    );
}

export default Carousel;