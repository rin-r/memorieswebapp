import React from 'react';
import { useNavigate } from 'react-router-dom';

function Navigator({}) {

    const navigate = useNavigate();

    function signUpButton() {
        navigate("/login");
    }

    return (
        <nav className="navbar navbar-expand-sm bg-white">
            <div className="container-xxl">

                <div className="navbar-brand">
                    <img className="me-1" src="logo.svg" alt="Mymemo logo" width="40" height="40" />
                    <span className="align-middle">
                        Memo
                    </span>
                </div>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" aria-current="page" href="#about">About</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#features">Features</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#subscription">Subscription</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#contact">Contact</a>
                        </li>
                        <li className="nav-item d-sm-none">
                            <a className="nav-link text-primary" href="/login">Sign in</a>
                        </li>
                        <li className="nav-item d-none d-sm-inline ms-2">
                            <button className="btn btn-primary" type='button' onClick={signUpButton}>Sign in</button>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    );
}

export default Navigator;