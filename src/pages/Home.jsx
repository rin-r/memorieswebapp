import React from 'react';
import Navigator from '../components/Navigator';
import Carousel from '../components/Carousel';
import Map from '../components/Map';

function Home({ }) {

    const footerMapCoords = [50.46448694116918, 30.519398724689896];

    const carouselData = [
        {
            imgSource: "https://images.inc.com/uploaded_files/image/1920x1080/getty_129714169_970647970450099_67857.jpg",
            label: "Store Memories",
            description: "Capture and cherish your memories in one place, creating a timeless collection that reflects the chapters of your life.",
            imgAlt: "Photos hanging on a rope"
        },
        {
            imgSource: "https://images.pexels.com/photos/3184292/pexels-photo-3184292.jpeg?cs=srgb&dl=pexels-fauxels-3184292.jpg&fm=jpg",
            label: "Share Plans",
            description: "Whether it's a trip, a project, or a personal goal, we help you organize your plans and keep track of your journey towards success.",
            imgAlt: "Memories"
        },
        {
            imgSource: "https://media.istockphoto.com/id/1459104303/photo/hand-man-holding-illuminated-lightbulb-idea-innovation-and-inspiration-with-glowing-virtual.jpg?s=612x612&w=0&k=20&c=xtoa7HUnHt5vxK0LStF67Hs3j4MhXmiM6_k4oxoDr3A=",
            label: "Express Ideas",
            description: "Share your thoughts with the world. From fleeting moments to profound reflections, share and explore your ideas effortlessly.",
            imgAlt: "Memories"
        },
    ]

    return (
        <>
            <Navigator />

            {/* carousel section */}
            <section className='container-xxl p-0'>
                <Carousel data={carouselData} />
            </section>

            {/* about us section */}
            <section className="container-xxl pt-5 px-5 bg-white" id="about" >
                <div className="row text-muted text-center justify-content-center">
                    <div className="col-12 mb-5">
                        <h1 className='display-5 text-black'>About Us</h1>
                    </div>
                    <div className="col-12 mb-3">
                        <p className='fs-5'>
                            At Memo, we believe in the power of sharing and connecting through the written word. Life is a collection of moments, plans, and experiences, and we provide a platform for you to capture and share those unique aspects of your journey.
                        </p>
                    </div>
                    <div className="col-12 mb-3">
                        <h3>Our Mission</h3>
                        <p className='fs-5'>
                            Our mission is to create a space where individuals can express themselves freely, share their thoughts, plans, and cherished moments with the world. We understand that life is a tapestry of experiences, and each story is a thread that contributes to the richness of our collective narrative.
                        </p>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <h3>What Sets Us Apart</h3>
                        </div>
                        <div className="row-lg-12 d-md-flex justify-content-evenly">
                            <div className="col-12 col-md-5">
                                <h4 className="lead">Easy and Intuitive</h4>
                                <p>
                                    We've designed Memo to be user-friendly and intuitive, ensuring that you can effortlessly navigate through the platform. Whether you're a seasoned writer or just starting to share your stories, our interface is tailored to make the process easy and enjoyable.
                                </p>
                            </div>
                            <div className="col-12 col-md-5">
                                <h4 className="lead">Secure and Private</h4>
                                <p>
                                    Your privacy and security are our top priorities. We employ robust security measures to protect your data, and you have control over the visibility of your posts. Share your moments with the world, or keep them private – it's entirely up to you.
                                </p>
                            </div>
                        </div>
                        <div className="row-lg-12 d-md-flex justify-content-evenly">
                            <div className="col-12 col-md-5">
                                <h4 className="lead">Community-driven</h4>
                                <p>
                                    Memo is more than just a platform, it's a community. Connect with like-minded individuals, discover new perspectives, and engage in meaningful conversations. Our community is diverse, supportive, and ready to embrace your unique voice.
                                </p>
                            </div>
                            <div className="col-12 col-md-5">
                                <h4 className="lead">Join Us on the Journey</h4>
                                <p>
                                    Whether you're here to document your daily life, share your plans for the future, or reflect on past experiences, Memo is the canvas for your digital expression. Join us on this journey of self-discovery, connection, and storytelling.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            {/* features section */}
            <section className="container-xxl" id="features">
                <div className="row justify-content-center">

                    <div className="col-12 py-5 bg-white text-center">
                        <h1 className="display-5 m-0 text-dark">Features</h1>
                    </div>

                    <div className="row my-3 text-lg-start justify-content-evenly text-center text-muted">
                        <div className="col-lg-3 p-3 mb-3 bg-white border rounded align-self-center">
                            <h3 className="text-primary">Create a Profile</h3>
                            <p className="m-0">
                                Personalize your experience! Create a profile, share your story, and connect with our community in just a few clicks.
                            </p>
                        </div>
                        <div className="col-lg-3 p-3 mb-3 bg-white border rounded">
                            <h3 className="text-primary">Comment on Everything</h3>
                            <p className="m-0">
                                Engage and express! Comment on posts, share thoughts, and be part of the conversation. Join the community – your voice matters!
                            </p>
                        </div>
                        <div className="col-lg-3 p-3 mb-3 bg-white border rounded align-self-center">
                            <h3 className="text-primary">Publish Posts</h3>
                            <p className="m-0">
                                Share your moments instantly! Publish posts and let your story unfold on Memo. Start expressing yourself now!
                            </p>
                        </div>
                    </div>

                </div>
            </section>

            {/* subscription section */}
            <section className="container-xxl py-5 bg-white" id="subscription">
                <div className="row g-4 justify-content-center align-items-center">

                    <div className="col-md-5 text-center d-none d-md-block">
                        <img src="/labels1.png" className="img-fluid" alt="Subscription promo" />
                    </div>

                    <div className="col-md-5 text-center">

                        <h1 className="display-5">Subscription</h1>
                        <p className="lead text-muted">Become a supporter and get additional benifits!</p>

                        <div className="card border-2 rounded border-primary">
                            <div className="card-body">
                                <h4 className="fs-2 mb-3 text-primary">Premium</h4>
                                <ul className="text-muted text-start">
                                    <li>Flaunt your premium status with a distinctive badge on your profile</li>
                                    <li>Enjoy a seamless and distraction-free experience without any advertisements</li>
                                    <li>Never worry about running out of space for your memories, write notes up to 100,000 symbols</li>
                                    <li>Premium members can enjoy a suite of customization options, allowing you to express yourself however they want</li>
                                </ul>
                                <div className="text-primary">only for</div>
                                <div className="display-4 text-primary">
                                    4.99$
                                    <span className="fs-5">/month</span>
                                </div>
                                <button className="btn btn-primary mt-3 w-100">Subscribe</button>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            {/* contact us section */}
            <section className="container-xxl bg-white" id="contact">
                <div className="row pb-5 justify-content-center">

                    <div className="col-12 pb-5 text-center">
                        <h1 className="display-5 m-0 text-dark">Contact Us</h1>
                    </div>

                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <form>
                                <div className="row mb-3 justify-content-between">
                                    <div className="col-6">
                                        <label htmlFor="userName" className="form-label">Name</label>
                                        <input type="text" className="form-control" id="userName" />
                                    </div>
                                    <div className="col-6">
                                        <label htmlFor="userSurname" className="form-label">Surname</label>
                                        <input type="text" className="form-control" id="userSurname" />
                                    </div>
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="userEmail" className="form-label">Email</label>
                                    <input type="email" className="form-control" id="userEmail" />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="userContent" className="form-label">Content</label>
                                    <textarea className="form-control" id="userContent" style={{ minHeight: '200px' }} />
                                </div>

                                <button className="btn btn-outline-primary w-100" type='submit'>Send</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>

            {/* footer section */}
            <section className="container-xxl bg-primary">
                <div className="row p-3">
                    <div className="col-lg-2 pb-3 mb-lg-4">
                        <div className="row fs-5 d-lg-grid text-center text-lg-start">
                            <a className='col-sm-3 col-lg-12 text-white text-decoration-none' href="#about">About</a>
                            <a className='col-sm-3 col-lg-12 text-white text-decoration-none' href="#features">Features</a>
                            <a className='col-sm-3 col-lg-12 text-white text-decoration-none' href="#subscription">Subscription</a>
                            <a className='col-sm-3 col-lg-12 text-white text-decoration-none' href="#contact">Contact</a>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <Map coordinates={footerMapCoords} />
                    </div>
                    <div className="col-lg-5 text-white text-center text-lg-start">
                        <p className="display-5 my-2">
                            Memo App
                        </p>
                        <p className='fs-6 m-0'>
                            Hryhoriya Skovorody St, 2, Kyiv, 04655<br />
                            Support:<br />
                            <a href='mailto:support@memo.com' className='text-white text-decoration-underline'>support@memo.com</a>
                        </p>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Home;