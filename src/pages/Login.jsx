import React, { useState } from 'react';
import "../css/loginPage.css"

function Login({ }) {

    const [curtain, setCurtain] = useState({ "coversSignUp": true });

    function changeCurtainSide() {
        setCurtain({'coversSignUp': !curtain.coversSignUp});
    }

    return (
        <>
            <div id="loginPageContainer" className="container-fluid justify-content-center align-items-center">
                <div className="row h-100 align-content-center">
                    <div className="col-12 pb-5 d-flex justify-content-center align-items-center">
                        <img src="logo.svg" alt="Logo" id="loginLogo" />
                        <span className='display-2'>Memo</span>
                    </div>
                    <div className="col-6 mb-5 position-relative align-self-center">
                        <form className='d-flex flex-column justify-content-center'>
                            <h3 className='text-center'>Sign In</h3>
                            <div className="mb-2">
                                <label htmlFor="signInEmail" className="form-label">Email</label>
                                <input type="email" className="form-control" id="signInEmail"></input>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="signInPassword" className="form-label">Password</label>
                                <input type="password" className="form-control" id="signInPassword"></input>
                            </div>
                            <div className="mb-2 form-check">
                                <input type="checkbox" className="form-check-input" id="rememberMe" />
                                <label className="form-check-label" htmlFor="rememberMe">Remember me</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Sign in</button>
                        </form>
                        <button className="btn btn-link text-muted w-100" onClick={changeCurtainSide}>Don't have an account</button>
                    </div>
                    <div className="col-6 mb-5 position-relative align-self-center">

                        <div
                            id="loginCurtains"
                            className="position-absolute w-100 h-100 bg-primary rounded-5"
                            style={{
                                transform: `translateX(${curtain.coversSignUp ? "-3%" : "-103%"})`
                            }}
                        >
                        </div>


                        <form className='d-flex flex-column justify-content-center'>
                            <h3 className='text-center'>Sign Up</h3>
                            <div className="row mb-2">
                                <div className="col-6">
                                    <label htmlFor="signInEmail" className="form-label">Name</label>
                                    <input type="email" className="form-control" id="signInEmail"></input>
                                </div>
                                <div className="col-6">
                                    <label htmlFor="signInEmail" className="form-label">Surname</label>
                                    <input type="email" className="form-control" id="signInEmail"></input>
                                </div>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="signInEmail" className="form-label">Username</label>
                                <input type="email" className="form-control" id="signInEmail"></input>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="signInEmail" className="form-label">Email</label>
                                <input type="email" className="form-control" id="signInEmail"></input>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="signInPassword" className="form-label">Password</label>
                                <input type="password" className="form-control" id="signInPassword"></input>
                            </div>
                            <div className="mb-4">
                                <label htmlFor="signInPassword" className="form-label">Confirm Password</label>
                                <input type="password" className="form-control" id="signInPassword"></input>
                            </div>
                            <button type="submit" className="btn btn-primary">Sign up</button>
                        </form>
                        <button className="btn btn-link text-muted w-100" onClick={changeCurtainSide}>Already have an account</button>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Login;